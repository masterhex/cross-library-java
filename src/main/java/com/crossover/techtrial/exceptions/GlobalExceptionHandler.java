package com.crossover.techtrial.exceptions;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Component
public class GlobalExceptionHandler {

  private static final Logger LOG = LoggerFactory.getLogger(GlobalExceptionHandler.class);
  private static final String STANDARD_MESSAGE= "Exception: Unable to process this request. {}";
  private static final String MESSAGE= "message";
  private static final String STATUS= "status";

  /**
   * Global Exception handler for all exceptions.
   */
  @ExceptionHandler
  public ResponseEntity<AbstractMap.SimpleEntry<String, String>> handle(Exception exception) {
    // general exception
    LOG.error(STANDARD_MESSAGE, exception);
    AbstractMap.SimpleEntry<String, String> response =
        new AbstractMap.SimpleEntry<>(MESSAGE, "Unable to process this request.");
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
  }

  @ExceptionHandler(ForbiddenException.class)
  public ResponseEntity<Map<String, String>> handleForbidden(ForbiddenException exception) {
    // general exception
    LOG.error(STANDARD_MESSAGE, exception);
    Map<String, String> response =
            new HashMap<>();
    response.put(MESSAGE, exception.getMessage());
    response.put(STATUS, "403");
    return ResponseEntity.status(HttpStatus.FORBIDDEN).body(response);
  }

  @ExceptionHandler(IllegalArgumentException.class)
  public ResponseEntity<Map<String, String>>  handleIllegalArgument(IllegalArgumentException exception) {
    // general exception
    LOG.error(STANDARD_MESSAGE, exception.getMessage());
    Map<String, String> response =
            new HashMap<>();
    response.put(MESSAGE, exception.getMessage());
    response.put(STATUS, "400");
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
  }

  @ExceptionHandler(NotFoundException.class)
  public ResponseEntity<Map<String, String>>  handleNotFound(NotFoundException exception) {
    // general exception
    LOG.error(STANDARD_MESSAGE, exception.getMessage());
    Map<String, String> response =
            new HashMap<>();
    response.put(MESSAGE, exception.getMessage());
    response.put(STATUS, "404");
    return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
  }
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<Map<String, String>>  handleNotValid(NotFoundException exception) {
    // general exception
    LOG.error(STANDARD_MESSAGE, exception.getMessage());
    Map<String, String> response =
            new HashMap<>();
    response.put(MESSAGE, exception.getMessage());
    response.put(STATUS, "403");
    return ResponseEntity.status(HttpStatus.FORBIDDEN).body(response);
  }
}
