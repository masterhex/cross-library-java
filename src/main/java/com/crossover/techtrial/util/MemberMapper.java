package com.crossover.techtrial.util;

import com.crossover.techtrial.domain.entity.MemberEntity;
import com.crossover.techtrial.domain.model.Member;

import java.util.List;
import java.util.stream.Collectors;


public class MemberMapper {

    public static Member entityToModel(MemberEntity entity) {
        if (entity == null)
            return null;
        return Member.builder().email(entity.getEmail())
                .id(entity.getId()).membershipStartDate(entity.getMembershipStartDate())
                .membershipStatus(entity.getMembershipStatus()).name(entity.getName())
                .build();
    }

    public static MemberEntity modelToEntity(Member model) {
        if (model == null)
            return null;
        return MemberEntity.builder().email(model.getEmail())
                .id(model.getId()).membershipStartDate(model.getMembershipStartDate())
                .membershipStatus(model.getMembershipStatus()).name(model.getName())
                .build();
    }

    public static List<Member> entityListToModelList(List<MemberEntity> entityList) {
        if (entityList == null)
            return null;
        return entityList.stream().map(MemberMapper::entityToModel).collect(Collectors.toList());
    }

    public static List<MemberEntity> modelListToEntityList(List<Member> modelList) {
        if (modelList == null)
            return null;
        return modelList.stream().map(MemberMapper::modelToEntity).collect(Collectors.toList());
    }
}
