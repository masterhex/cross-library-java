package com.crossover.techtrial.util;

import com.crossover.techtrial.domain.entity.BookEntity;
import com.crossover.techtrial.domain.model.Book;

import java.util.List;
import java.util.stream.Collectors;

public class BookMapper {

    public static Book entityToModel(BookEntity entity) {
        if (entity == null)
            return null;

        return Book.builder().id(entity.getId()).title(entity.getTitle()).build();
    }

    public static BookEntity modelToEntity(Book model) {
        if (model == null)
            return null;
        return BookEntity.builder().id(model.getId()).title(model.getTitle()).build();
    }

    public static List<Book> entityListToModelList(List<BookEntity> entityList) {
        if (entityList == null)
            return null;
        return entityList.stream().map(BookMapper::entityToModel).collect(Collectors.toList());
    }

    public static List<BookEntity> modelListToEntityList(List<Book> modelList) {
        if (modelList == null)
            return null;
        return modelList.stream().map(BookMapper::modelToEntity).collect(Collectors.toList());
    }
}
