package com.crossover.techtrial.util;

import com.crossover.techtrial.domain.entity.TransactionEntity;
import com.crossover.techtrial.domain.model.Transaction;

import java.util.List;
import java.util.stream.Collectors;

public class TransactionMapper {

    public static Transaction entityToModel(TransactionEntity entity) {
        if (entity == null)
            return null;
        return Transaction.builder().id(entity.getId())
                .book(BookMapper.entityToModel(entity.getBookEntity()))
                .dateOfIssue(entity.getDateOfIssue())
                .dateOfReturn(entity.getDateOfReturn())
                .member(MemberMapper.entityToModel(entity.getMemberEntity()))
                .build();
    }

    public static TransactionEntity modelToEntity(Transaction model) {
        if (model == null)
            return null;
        return TransactionEntity.builder().id(model.getId())
                .bookEntity(BookMapper.modelToEntity(model.getBook()))
                .dateOfIssue(model.getDateOfIssue())
                .dateOfReturn(model.getDateOfReturn())
                .memberEntity(MemberMapper.modelToEntity(model.getMember()))
                .build();
    }

    public static List<Transaction> entityListToModelList(List<TransactionEntity> entityList) {
        if (entityList == null)
            return null;
        return entityList.stream().map(TransactionMapper::entityToModel).collect(Collectors.toList());
    }

    public static List<TransactionEntity> modelListToEntityList(List<Transaction> modelList) {
        if (modelList == null)
            return null;
        return modelList.stream().map(TransactionMapper::modelToEntity).collect(Collectors.toList());
    }
}
