/**
 * 
 */
package com.crossover.techtrial.controller;

import com.crossover.techtrial.domain.model.Transaction;
import com.crossover.techtrial.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author kshah
 *
 */
@RestController
public class TransactionController {

  @Autowired
  private TransactionService transactionService;
  /*
   * PLEASE DO NOT CHANGE SIGNATURE OR METHOD TYPE OF END POINTS
   * Example Post Request :  { "bookId":1,"memberId":33 }
   */
  @PostMapping(path = "/api/transaction")
  public ResponseEntity<Transaction> issueBookToMember(@RequestBody Map<String, Long> params){
    // TODO Do we really need this Map? Maybe we should use DTO instead?
    Long bookId = params.get("bookId");
    Long memberId = params.get("memberId");
    Transaction transaction = transactionService.saveByBookIdAndMemberId(bookId, memberId);
    return ResponseEntity.ok().body(transaction);
  }
  /*
   * PLEASE DO NOT CHANGE SIGNATURE OR METHOD TYPE OF END POINTS
   */
  @PatchMapping(path= "/api/transaction/{transaction-id}/return")
  public ResponseEntity<Transaction> returnBookTransaction(@PathVariable(name="transaction-id") Long transactionId){
    Transaction transaction = transactionService.updateReturnDate(transactionId);
    return ResponseEntity.ok().body(transaction);
  }

}
