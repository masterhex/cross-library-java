/**
 * 
 */
package com.crossover.techtrial.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.crossover.techtrial.domain.model.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.crossover.techtrial.dto.TopMemberDTO;
import com.crossover.techtrial.service.MemberService;

import javax.validation.Valid;

/**
 * 
 * @author crossover
 */
@CrossOrigin
@RestController
@RequestMapping("/api/member")
public class MemberController {
  
  @Autowired
  private MemberService memberService;
  /*
   * PLEASE DO NOT CHANGE SIGNATURE OR METHOD TYPE OF END POINTS
   */
  @PostMapping("")
  public ResponseEntity<Member> register(@Valid @RequestBody Member member) {
    return ResponseEntity.ok(memberService.save(member));
  }
  
  /*
   * PLEASE DO NOT CHANGE API SIGNATURE OR METHOD TYPE OF END POINTS
   */
  @GetMapping("")
  public ResponseEntity<List<Member>> getAll() {
    return ResponseEntity.ok(memberService.findAll());
  }
  
  /*
   * PLEASE DO NOT CHANGE API SIGNATURE OR METHOD TYPE OF END POINTS
   */
  @GetMapping("/{member-id}")
  public ResponseEntity<Member> getMemberById(@PathVariable(name="member-id", required=true)Long memberId) {
    Member member = memberService.findById(memberId);
    if (member != null) {
      return ResponseEntity.ok(member);
    }
    return ResponseEntity.notFound().build();
  }
  
  
  /**
   * This API returns the top 5 members who issued the most books within the search duration. 
   * Only books that have dateOfIssue and dateOfReturn within the mentioned duration should be counted.
   * Any issued book where dateOfIssue or dateOfReturn is outside the search, should not be considered. 
   * 
   * DONT CHANGE METHOD SIGNATURE AND RETURN TYPES
   * @return
   */
  @GetMapping("/top-member")
  public ResponseEntity<List<TopMemberDTO>> getTopMembers(
      @RequestParam(value="startTime", required=true) @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss") LocalDateTime startTime,
      @RequestParam(value="endTime", required=true) @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss") LocalDateTime endTime){
    List<TopMemberDTO> topDrivers = memberService.findTopMembers(startTime, endTime);

    
    return ResponseEntity.ok(topDrivers);
    
  }
  
}
