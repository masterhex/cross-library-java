/**
 * 
 */
package com.crossover.techtrial.controller;

import java.util.List;

import com.crossover.techtrial.domain.entity.BookEntity;
import com.crossover.techtrial.domain.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.crossover.techtrial.service.BookService;

/**
 * BookController for Book related APIs.
 * @author crossover
 *
 */
@RestController
@RequestMapping("/api/book")
public class BookController {
  
  @Autowired 
  private BookService bookService;
  
  /*
   * PLEASE DO NOT CHANGE API SIGNATURE OR METHOD TYPE OF END POINTS
   */
  @GetMapping("")
  public ResponseEntity<List<Book>> getBooks() {
    return ResponseEntity.ok(bookService.findAll());
  }

  /*
   * PLEASE DO NOT CHANGE API SIGNATURE OR METHOD TYPE OF END POINTS
   */
  @PostMapping("")
  public ResponseEntity<Book> saveBook(@RequestBody Book book) {
    return ResponseEntity.ok(bookService.save(book));
  }
  
  /*
   * PLEASE DO NOT CHANGE API SIGNATURE OR METHOD TYPE OF END POINTS
   */
  @GetMapping("/{book-id}")
  public ResponseEntity<Book> getRideById(@PathVariable(name="book-id",required=true)Long bookId){
    Book book = bookService.findById(bookId);
    if (book !=null)
      return ResponseEntity.ok(book);
    return ResponseEntity.notFound().build();
  }

  
}
