/**
 * 
 */
package com.crossover.techtrial.dto;

import lombok.*;

/**
 * @author crossover
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class TopMemberDTO {

  private Long memberId;

  private String name;

  private String email;

  private Integer bookCount;

}
