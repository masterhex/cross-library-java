package com.crossover.techtrial.repositories;


import com.crossover.techtrial.domain.entity.TopMemberEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * I have decided to take all top members by one query for the performance purpose.
 * I could've take all the details separately from the existing repos. But...
 */
@RestResource(exported=false)
public interface TopMemberRepository extends PagingAndSortingRepository<TopMemberEntity, Long> {
    @Transactional
    @Query(value = "SELECT m.id, m.email, m.name, count(t.book_id) as count  FROM member m\n" +
            "INNER JOIN transaction t ON m.id=t.member_id\n" +
            "  WHERE t.date_of_issue BETWEEN ?1 AND ?2\n" +
            "  AND t.date_of_return BETWEEN ?1 AND ?2\n" +
            "  AND m.membership_status='ACTIVE'\n" +
            "GROUP BY m.id ORDER BY count(t.book_id) DESC LIMIT ?3", nativeQuery = true)
    List<TopMemberEntity> findTopMembers(LocalDateTime startTime, LocalDateTime endTime, Integer limit);
}
