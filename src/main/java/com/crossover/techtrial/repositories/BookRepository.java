/**
 * 
 */
package com.crossover.techtrial.repositories;

import com.crossover.techtrial.domain.entity.BookEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;
import java.util.Optional;

/**
 * @author crossover
 *
 */
@RestResource(exported = false)
public interface BookRepository extends CrudRepository<BookEntity, Long> {
    Optional<BookEntity> findById(Long id);
    List<BookEntity> findAll();
}
