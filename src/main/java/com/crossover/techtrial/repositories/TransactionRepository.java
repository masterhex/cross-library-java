/**
 * 
 */
package com.crossover.techtrial.repositories;

import com.crossover.techtrial.domain.entity.TransactionEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * @author crossover
 *
 */
@RestResource(exported = false)
public interface TransactionRepository extends CrudRepository<TransactionEntity, Long> {
    Optional<TransactionEntity> findById(Long id);
    List<TransactionEntity> findAll();
    @Transactional
    @Query(value = "SELECT count(id) FROM transaction t WHERE member_id=?1 AND date_of_return IS NOT NULL", nativeQuery = true)
    Integer countByMemberIdWhereEndDateNotNull(Long memberId);

    @Transactional
    @Query(value = "SELECT count(id) FROM transaction t WHERE book_id=?1 AND date_of_return IS NOT NULL", nativeQuery = true)
    Integer countByBookIdWhereEndDateNotNull(Long bookId);
}
