/**
 * 
 */
package com.crossover.techtrial.domain.entity;

import com.crossover.techtrial.domain.enums.MembershipStatus;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author kshah
 *
 */
@Entity
@Table(name = "member")
@Getter
@Setter
@EqualsAndHashCode
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MemberEntity implements Serializable{
  
  private static final long serialVersionUID = 9045098179799205444L;
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "name")
  private String name;

  @Column(name = "email")
  private String email;
  
  @Enumerated(EnumType.STRING)
  private MembershipStatus membershipStatus;
  
  @Column(name = "membership_start_date")
  private LocalDateTime membershipStartDate;

}
