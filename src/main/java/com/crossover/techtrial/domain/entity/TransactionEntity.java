/**
 * 
 */
package com.crossover.techtrial.domain.entity;

import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * @author kshah
 *
 */
@Entity
@Table(name="transaction")
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionEntity implements Serializable {

  private static final long serialVersionUID = 8951221480021840448L;
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  
  @OneToOne
  @JoinColumn(name = "book_id", referencedColumnName = "id")
  private BookEntity bookEntity;
  
  @OneToOne
  @JoinColumn(name="member_id", referencedColumnName="id")
  private MemberEntity memberEntity;
  //Date and time of issuance of this bookEntity
  @Column(name="date_of_issue")
  private LocalDateTime dateOfIssue;
  
  //Date and time of return of this bookEntity
  @Column(name="date_of_return")
  private LocalDateTime dateOfReturn;

}
