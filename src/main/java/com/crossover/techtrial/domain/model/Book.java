package com.crossover.techtrial.domain.model;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
@Builder
public class Book {
    private Long id;
    private String title;
}
