package com.crossover.techtrial.domain.model;

import com.crossover.techtrial.domain.enums.MembershipStatus;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;


@Data
@EqualsAndHashCode
@Builder
public class Member {
    private Long id;
    @Size(min = 2, max = 100, message = "The name's length should be from 2 to 100 ")
    @Pattern(regexp = "^[a-zA-Z][a-zA-Z0-9.,$;]+$")
    private String name;
    @NotNull
    @Email(message = "Invalid email field")
    private String email;

    private MembershipStatus membershipStatus;

    private LocalDateTime membershipStartDate;
}
