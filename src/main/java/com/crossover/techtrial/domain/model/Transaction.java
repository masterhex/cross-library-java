package com.crossover.techtrial.domain.model;


import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode
@Builder
public class Transaction {
    private Long id;

    private Book book;

    private Member member;

    private LocalDateTime dateOfIssue;

    private LocalDateTime dateOfReturn;
}
