/**
 * 
 */
package com.crossover.techtrial.domain.enums;

/**
 * @author kshah
 *
 */
public enum MembershipStatus {
  ACTIVE,
  INACTIVE,
  BLOCKED
}
