package com.crossover.techtrial.service;

import java.util.List;

public interface BasicService<T> {
    List<T> findAll();

    T save(T p);

    T findById(Long id);
}
