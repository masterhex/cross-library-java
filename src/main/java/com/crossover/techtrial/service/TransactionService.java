package com.crossover.techtrial.service;


import com.crossover.techtrial.domain.model.Transaction;

public interface TransactionService extends BasicService<Transaction> {
    Transaction saveByBookIdAndMemberId(Long bookId, Long memberId);
    Transaction updateReturnDate(Long transactionId);
}
