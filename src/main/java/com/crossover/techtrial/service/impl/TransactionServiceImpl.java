package com.crossover.techtrial.service.impl;

import com.crossover.techtrial.domain.entity.BookEntity;
import com.crossover.techtrial.domain.entity.MemberEntity;
import com.crossover.techtrial.domain.entity.TransactionEntity;
import com.crossover.techtrial.domain.model.Transaction;
import com.crossover.techtrial.exceptions.ForbiddenException;
import com.crossover.techtrial.exceptions.NotFoundException;
import com.crossover.techtrial.repositories.BookRepository;
import com.crossover.techtrial.repositories.MemberRepository;
import com.crossover.techtrial.repositories.TransactionRepository;
import com.crossover.techtrial.service.TransactionService;
import com.crossover.techtrial.util.TransactionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TransactionServiceImpl implements TransactionService {
    private static final Logger LOG = LoggerFactory.getLogger(TransactionServiceImpl.class);
    private static final Integer LIMIT_COUNT_OF_BOOKS = 5;

    private TransactionRepository repository;

    private BookRepository bookRepository;

    private MemberRepository memberRepository;

    public TransactionServiceImpl(@Autowired TransactionRepository repository,
                                  @Autowired BookRepository bookRepository,
                                  @Autowired MemberRepository memberRepository){
        this.repository = repository;
        this.bookRepository = bookRepository;
        this.memberRepository = memberRepository;
    }

    @Override
    public List<Transaction> findAll() {
        LOG.info("Getting all transactions");
        List<TransactionEntity> bookEntityList = repository.findAll();
        List<Transaction> books = TransactionMapper.entityListToModelList(bookEntityList);
        LOG.debug("Getting all transactions {}", books);
        return books;
    }

    @Override
    public Transaction save(Transaction transaction) {
        Assert.notNull(transaction, "transaction is required");
        LOG.info("Saving transaction");
        LOG.debug("Saving transaction = {}", transaction);
        transaction.setDateOfIssue(LocalDateTime.now());
        transaction = TransactionMapper.entityToModel(repository.save(TransactionMapper.modelToEntity(transaction)));
        LOG.debug("transaction after save = {}", transaction);
        return transaction;
    }

    @Override
    public Transaction findById(Long id) {
        Assert.notNull(id, "id is required");
        LOG.info("Finding transaction by id = {}", id);
        Optional<TransactionEntity> transactionEntityOptional = repository.findById(id);

        return TransactionMapper.entityToModel(transactionEntityOptional.orElse(null));
    }

    @Override
    public Transaction saveByBookIdAndMemberId(Long bookId, Long memberId) {
        Assert.notNull(memberId, "memberId is required");
        Assert.notNull(bookId, "bookId is required");

        checkBooksCountOfMemberForLimit(memberId);
        checkIsBookIsAlreadyIssued(bookId);

        Optional<BookEntity> bookEntity = bookRepository.findById(bookId);
        Optional<MemberEntity> memberEntity = memberRepository.findById(memberId);

        checkIfBookAndMemberExist(bookEntity, memberEntity, bookId, memberId);

        TransactionEntity transactionEntity = TransactionEntity.builder()
        .bookEntity(bookEntity.get())
        .memberEntity(memberEntity.get())
                .dateOfIssue(LocalDateTime.now())
                .build();
        Transaction transaction = TransactionMapper.entityToModel(repository.save(transactionEntity));
        LOG.debug("Saved transaction = {}", transaction);
        return transaction;
    }

    @Override
    public Transaction updateReturnDate(Long transactionId) {
        Optional<TransactionEntity> transactionEntityOptional = repository.findById(transactionId);
        transactionEntityOptional.ifPresent(s-> {
            checkIsBookIsAlreadyReturned(s);
            s.setDateOfReturn(LocalDateTime.now());
            repository.save(s);
        });
        if (!transactionEntityOptional.isPresent()){
            LOG.error("Exception: transaction not found {}", transactionId);
            throw new NotFoundException("Transaction not found");
        }
        return TransactionMapper.entityToModel(transactionEntityOptional.orElse(null));
    }

    private void checkBooksCountOfMemberForLimit(Long memberId){
        Integer count = repository.countByMemberIdWhereEndDateNotNull(memberId);
        LOG.info("member {} count of books are {}", memberId, count);
        if (count >= LIMIT_COUNT_OF_BOOKS){
            LOG.error("Exception: member {} has max count of books {}", memberId , count);
            throw new ForbiddenException("There is max count of issuing books");
        }
    }

    private void checkIsBookIsAlreadyIssued(Long bookId){
        Integer count = repository.countByBookIdWhereEndDateNotNull(bookId);
        LOG.info("book {} issue count {}", bookId, count);
        if (count > 0){
            LOG.error("Exception: book {} is already issued {}", bookId , count);
            throw new ForbiddenException("The book is already issued");
        }
    }

    private void checkIsBookIsAlreadyReturned(TransactionEntity transactionEntity){
        LOG.info("checking if book {} is returned in {}", transactionEntity.getBookEntity().getId() , transactionEntity.getId());
        if (transactionEntity.getDateOfReturn()!=null){
            LOG.error("Exception: book {} is already returned in transaction {}", transactionEntity.getBookEntity().getId() , transactionEntity.getId());
            throw new ForbiddenException("The book is already returned in this transaction");
        }
    }

    private void checkIfBookAndMemberExist(Optional<BookEntity> bookEntity,
                                           Optional<MemberEntity> memberEntity,
                                           Long bookId, Long memberId){
        if(!bookEntity.isPresent()){
            LOG.error("Exception: book not found with id {}", bookId);
            throw new NotFoundException("Book not found");
        }
        if(!memberEntity.isPresent()){
            LOG.error("Exception: member not found with id {}", memberId);
            throw new NotFoundException("Member not found");
        }
    }
}
