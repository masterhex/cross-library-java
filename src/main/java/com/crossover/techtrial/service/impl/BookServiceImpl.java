/**
 *
 */
package com.crossover.techtrial.service.impl;

import com.crossover.techtrial.domain.entity.BookEntity;
import com.crossover.techtrial.domain.model.Book;
import com.crossover.techtrial.repositories.BookRepository;
import com.crossover.techtrial.service.BookService;
import com.crossover.techtrial.util.BookMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;

/**
 * @author crossover
 */
@Service
public class BookServiceImpl implements BookService {
    private static final Logger LOG = LoggerFactory.getLogger(BookServiceImpl.class);

    private BookRepository bookRepository;

    public BookServiceImpl(@Autowired BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<Book> findAll() {
        LOG.info("Getting all books");
        List<BookEntity> bookEntityList = bookRepository.findAll();
        List<Book> books = BookMapper.entityListToModelList(bookEntityList);
        LOG.debug("Getting all books {}", books);
        return books;

    }

    public Book save(Book book) {
        Assert.notNull(book, "book is required");
        Assert.notNull(book.getTitle(), "title is required");
        LOG.debug("Saving book {}", book);
        Book result = BookMapper.entityToModel(bookRepository.save(BookMapper.modelToEntity(book)));
        LOG.debug("Book after save {}", result);
        return result;
    }

    @Override
    public Book findById(Long bookId) {
        Assert.notNull(bookId, "bookId is required");
        LOG.info("Finding book by id = {}", bookId);
        Optional<BookEntity> bookEntityOptional = bookRepository.findById(bookId);

        return BookMapper.entityToModel(bookEntityOptional.orElse(null));
    }

}
