/**
 *
 */
package com.crossover.techtrial.service.impl;

import com.crossover.techtrial.domain.entity.MemberEntity;
import com.crossover.techtrial.domain.model.Member;
import com.crossover.techtrial.dto.TopMemberDTO;
import com.crossover.techtrial.repositories.MemberRepository;
import com.crossover.techtrial.repositories.TopMemberRepository;
import com.crossover.techtrial.service.MemberService;
import com.crossover.techtrial.util.MemberMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author crossover
 */
@Service
public class MemberServiceImpl implements MemberService {
    private static final Logger LOG = LoggerFactory.getLogger(MemberServiceImpl.class);
    private static final Integer TOP_MEMBER_LIMIT = 5;

    private MemberRepository memberRepository;

    private TopMemberRepository topMemberRepository;

    public MemberServiceImpl(@Autowired MemberRepository memberRepository,
                             @Autowired TopMemberRepository topMemberRepository) {
        this.memberRepository = memberRepository;
        this.topMemberRepository = topMemberRepository;
    }

    @Override
    public Member save(Member member) {
        Assert.notNull(member, "member is required");
        LOG.info("Saving member");
        LOG.debug("Saving member = {}", member);
        member = MemberMapper.entityToModel(memberRepository.save(MemberMapper.modelToEntity(member)));
        LOG.debug("member after save = {}", member);
        return member;
    }

    @Override
    public Member findById(Long memberId) {
        Assert.notNull(memberId, "memberId is required");
        LOG.info("Getting member by id = {}", memberId);
        Optional<MemberEntity> optionalMember = memberRepository.findById(memberId);

        return MemberMapper.entityToModel(optionalMember.orElse(null));
    }

    @Override
    public List<Member> findAll() {
        LOG.info("Getting all members");
        List<Member> members = MemberMapper.entityListToModelList(memberRepository.findAll());
        LOG.debug("findAll members {}", members);
        return members;
    }

    @Override
    public List<TopMemberDTO> findTopMembers(LocalDateTime startTime, LocalDateTime endTime) {
        Assert.notNull(startTime, "startTime is required");
        Assert.notNull(endTime, "endTime is required");
        LOG.info("Searching for top members by startTime {} and endTime {}", startTime, endTime);
        List<TopMemberDTO> members = topMemberRepository.findTopMembers(startTime, endTime, TOP_MEMBER_LIMIT).stream()
                .map(s -> TopMemberDTO.builder().name(s.getName()).bookCount(s.getCount() != null ? s.getCount().intValue() : null)
                        .email(s.getEmail()).memberId(s.getId()).build()).collect(Collectors.toList());
        LOG.debug("Returning top members {}", members);
        return members;
    }
}
