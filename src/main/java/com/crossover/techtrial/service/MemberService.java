/**
 * 
 */
package com.crossover.techtrial.service;

import com.crossover.techtrial.domain.model.Member;
import com.crossover.techtrial.dto.TopMemberDTO;

import java.time.LocalDateTime;
import java.util.List;

/**
 * RideService for rides.
 * @author crossover
 *
 */
public interface MemberService extends BasicService<Member> {
    List<TopMemberDTO> findTopMembers(LocalDateTime startTime, LocalDateTime endTime);
}
