/**
 * 
 */
package com.crossover.techtrial.service;

import com.crossover.techtrial.domain.model.Book;

/**
 * BookService interface for Books.
 * @author cossover
 *
 */
public interface BookService extends BasicService<Book> {

}
