package com.crossover.techtrial.unit;


import com.crossover.techtrial.domain.entity.BookEntity;
import com.crossover.techtrial.domain.entity.MemberEntity;
import com.crossover.techtrial.domain.entity.TransactionEntity;
import com.crossover.techtrial.domain.model.Book;
import com.crossover.techtrial.domain.model.Member;
import com.crossover.techtrial.domain.model.Transaction;
import com.crossover.techtrial.util.BookMapper;
import com.crossover.techtrial.util.MemberMapper;
import com.crossover.techtrial.util.TransactionMapper;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class UtilTest {

    @Test
    public void testBookMapperShouldReturnModel(){
        BookEntity entity = BookEntity.builder().id(1L).title("TITLE").build();
        Book model = BookMapper.entityToModel(entity);
        Assert.assertNotNull(model);
        Assert.assertTrue(model.getId().equals(1L));
        Assert.assertTrue(model.getTitle().equals("TITLE"));

        List<BookEntity> list = new ArrayList<>();
        list.add(entity);
        List<Book> list2 = BookMapper.entityListToModelList(list);
        Assert.assertNotNull(list2);
        Assert.assertTrue(list2.size()==1);
        Assert.assertTrue(list2.get(0).getId().equals(1L));
        Assert.assertTrue(list2.get(0).getTitle().equals("TITLE"));
    }

    @Test
    public void testBookMapperShouldReturnEntity(){
        Book model = Book.builder().id(1L).title("TITLE").build();
        BookEntity entity = BookMapper.modelToEntity(model);
        Assert.assertNotNull(entity);
        Assert.assertTrue(entity.getId().equals(1L));
        Assert.assertTrue(entity.getTitle().equals("TITLE"));

        List<Book> list = new ArrayList<>();
        list.add(model);
        List<BookEntity> list2 = BookMapper.modelListToEntityList(list);
        Assert.assertNotNull(list2);
        Assert.assertTrue(list2.size()==1);
        Assert.assertTrue(list2.get(0).getId().equals(1L));
        Assert.assertTrue(list2.get(0).getTitle().equals("TITLE"));
    }

    @Test
    public void testMappersShouldReturnNull(){
        Assert.assertNull(BookMapper.entityToModel(null));
        Assert.assertNull(BookMapper.modelToEntity(null));
        Assert.assertNull(BookMapper.entityListToModelList(null));
        Assert.assertNull(BookMapper.modelListToEntityList(null));
        Assert.assertNull(MemberMapper.entityToModel(null));
        Assert.assertNull(MemberMapper.modelToEntity(null));
        Assert.assertNull(MemberMapper.entityListToModelList(null));
        Assert.assertNull(MemberMapper.modelListToEntityList(null));
    }

    @Test
    public void testMemberMapperShouldReturnModel(){
        MemberEntity entity = MemberEntity.builder().id(1L).name("TITLE").build();
        Member model = MemberMapper.entityToModel(entity);
        Assert.assertNotNull(model);
        Assert.assertTrue(model.getId().equals(1L));
        Assert.assertTrue(model.getName().equals("TITLE"));

        List<MemberEntity> list = new ArrayList<>();
        list.add(entity);
        List<Member> list2 = MemberMapper.entityListToModelList(list);
        Assert.assertNotNull(list2);
        Assert.assertTrue(list2.size()==1);
        Assert.assertTrue(list2.get(0).getId().equals(1L));
        Assert.assertTrue(list2.get(0).getName().equals("TITLE"));
    }

    @Test
    public void testMemberMapperShouldReturnEntity(){
        Member model = Member.builder().id(1L).name("TITLE").build();
        MemberEntity entity = MemberMapper.modelToEntity(model);
        Assert.assertNotNull(entity);
        Assert.assertTrue(entity.getId().equals(1L));
        Assert.assertTrue(entity.getName().equals("TITLE"));

        List<Member> list = new ArrayList<>();
        list.add(model);
        List<MemberEntity> list2 = MemberMapper.modelListToEntityList(list);
        Assert.assertNotNull(list2);
        Assert.assertTrue(list2.size()==1);
        Assert.assertTrue(list2.get(0).getId().equals(1L));
        Assert.assertTrue(list2.get(0).getName().equals("TITLE"));
    }

    @Test
    public void testTransactionMapperShouldReturnModel(){
        TransactionEntity entity = TransactionEntity.builder().id(1L).dateOfIssue(LocalDateTime.MIN).build();
        Transaction model = TransactionMapper.entityToModel(entity);
        Assert.assertNotNull(model);
        Assert.assertTrue(model.getId().equals(1L));
        Assert.assertTrue(entity.getDateOfIssue().equals(LocalDateTime.MIN));

        List<TransactionEntity> list = new ArrayList<>();
        list.add(entity);
        List<Transaction> list2 = TransactionMapper.entityListToModelList(list);
        Assert.assertNotNull(list2);
        Assert.assertTrue(list2.size()==1);
        Assert.assertTrue(list2.get(0).getId().equals(1L));
        Assert.assertTrue(list2.get(0).getDateOfIssue().equals(LocalDateTime.MIN));
    }

    @Test
    public void testTransactionMapperShouldReturnEntity(){
        Transaction model = Transaction.builder().id(1L).dateOfIssue(LocalDateTime.MIN).build();
        TransactionEntity entity = TransactionMapper.modelToEntity(model);
        Assert.assertNotNull(entity);
        Assert.assertTrue(entity.getId().equals(1L));
        Assert.assertTrue(entity.getDateOfIssue().equals(LocalDateTime.MIN));

        List<Transaction> list = new ArrayList<>();
        list.add(model);
        List<TransactionEntity> list2 = TransactionMapper.modelListToEntityList(list);
        Assert.assertNotNull(list2);
        Assert.assertTrue(list2.size()==1);
        Assert.assertTrue(list2.get(0).getId().equals(1L));
        Assert.assertTrue(list2.get(0).getDateOfIssue().equals(LocalDateTime.MIN));
    }

}
