package com.crossover.techtrial.unit;


import com.crossover.techtrial.domain.entity.BookEntity;
import com.crossover.techtrial.domain.entity.MemberEntity;
import com.crossover.techtrial.domain.entity.TopMemberEntity;
import com.crossover.techtrial.domain.model.Book;
import com.crossover.techtrial.domain.model.Member;
import com.crossover.techtrial.dto.TopMemberDTO;
import com.crossover.techtrial.repositories.BookRepository;
import com.crossover.techtrial.repositories.MemberRepository;
import com.crossover.techtrial.repositories.TopMemberRepository;
import com.crossover.techtrial.service.BookService;
import com.crossover.techtrial.service.MemberService;
import com.crossover.techtrial.service.impl.BookServiceImpl;
import com.crossover.techtrial.service.impl.MemberServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BookServiceTest {

    private BookService bookService;

    private BookRepository bookRepository;

    @Before
    public void setUp() {
        bookRepository = Mockito.mock(BookRepository.class);
        bookService = new BookServiceImpl(bookRepository);
    }


    @Test
    public void testShouldSavePositive() {
        doReturn(BookEntity.builder().build()).when(bookRepository).save(any());
        bookService.save(Book.builder().title("saving").build());
        verify(bookRepository, times(1)).save(any());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testShouldSaveThrowIllegalArgumentException() {
        bookService.save(null);
    }

    @Test
    public void testShouldFindByIdPositive() {
        String title = "John Doe";
        doReturn(Optional.ofNullable(BookEntity.builder().title(title).build())).when(bookRepository).findById(any());
        Book book = bookService.findById(1L);
        Assert.assertNotNull(book);
        Assert.assertTrue(book.getTitle().equals(title));
        verify(bookRepository, times(1)).findById(any());
    }

    @Test
    public void testShouldFindByIdPositiveReturnNull() {
        doReturn(Optional.ofNullable(null)).when(bookRepository).findById(any());
        Book book = bookService.findById(1L);
        Assert.assertNull(book);
        verify(bookRepository, times(1)).findById(any());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testShouldFindByIdThrow() {
        bookService.findById(null);
    }

    @Test
    public void testShouldFindAllPositive() {
        List<BookEntity> list = new ArrayList<>();
        list.add(BookEntity.builder().build());
        list.add(BookEntity.builder().build());
        doReturn(list).when(bookRepository).findAll();
        List<Book> books = bookService.findAll();
        Assert.assertNotNull(books);
        Assert.assertTrue(books.size() == 2);
        verify(bookRepository, times(1)).findAll();
    }

    @Test
    public void testShouldFindAllReturnNull() {
        doReturn(null).when(bookRepository).findAll();
        List<Book> books = bookService.findAll();
        Assert.assertNull(books);
        verify(bookRepository, times(1)).findAll();
    }

}
