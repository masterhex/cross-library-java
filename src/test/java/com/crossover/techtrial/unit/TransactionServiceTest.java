package com.crossover.techtrial.unit;


import com.crossover.techtrial.domain.entity.BookEntity;
import com.crossover.techtrial.domain.entity.MemberEntity;
import com.crossover.techtrial.domain.entity.TransactionEntity;
import com.crossover.techtrial.domain.model.Book;
import com.crossover.techtrial.domain.model.Transaction;
import com.crossover.techtrial.exceptions.ForbiddenException;
import com.crossover.techtrial.exceptions.NotFoundException;
import com.crossover.techtrial.repositories.BookRepository;
import com.crossover.techtrial.repositories.MemberRepository;
import com.crossover.techtrial.repositories.TransactionRepository;
import com.crossover.techtrial.service.BookService;
import com.crossover.techtrial.service.TransactionService;
import com.crossover.techtrial.service.impl.BookServiceImpl;
import com.crossover.techtrial.service.impl.TransactionServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceTest {

    private TransactionService transactionService;

    private BookRepository bookRepository;

    private TransactionRepository transactionRepository;

    private MemberRepository memberRepository;

    @Before
    public void setUp() {
        bookRepository = Mockito.mock(BookRepository.class);
        transactionRepository = Mockito.mock(TransactionRepository.class);
        memberRepository = Mockito.mock(MemberRepository.class);
        transactionService = new TransactionServiceImpl(transactionRepository, bookRepository, memberRepository);
    }


    @Test
    public void testShouldSavePositive() {
        doReturn(TransactionEntity.builder().build()).when(transactionRepository).save(any());
        transactionService.save(Transaction.builder().build());
        verify(transactionRepository, times(1)).save(any());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testShouldSaveThrowIllegalArgumentException() {
        transactionService.save(null);
    }

    @Test
    public void testShouldFindByIdPositive() {
        doReturn(Optional.ofNullable(TransactionEntity.builder().build())).when(transactionRepository).findById(any());
        Transaction transaction = transactionService.findById(1L);
        Assert.assertNotNull(transaction);
        verify(transactionRepository, times(1)).findById(any());
    }

    @Test
    public void testShouldFindByIdPositiveReturnNull() {
        doReturn(Optional.ofNullable(null)).when(transactionRepository).findById(any());
        Transaction transaction = transactionService.findById(1L);
        Assert.assertNull(transaction);
        verify(transactionRepository, times(1)).findById(any());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testShouldFindByIdThrow() {
        transactionService.findById(null);
    }

    @Test
    public void testShouldFindAllPositive() {
        List<TransactionEntity> list = new ArrayList<>();
        list.add(TransactionEntity.builder().build());
        list.add(TransactionEntity.builder().build());
        doReturn(list).when(transactionRepository).findAll();
        List<Transaction> transactions = transactionService.findAll();
        Assert.assertNotNull(transactions);
        Assert.assertTrue(transactions.size() == 2);
        verify(transactionRepository, times(1)).findAll();
    }

    @Test
    public void testShouldFindAllReturnNull() {
        doReturn(null).when(transactionRepository).findAll();
        List<Transaction> transactions = transactionService.findAll();
        Assert.assertNull(transactions);
        verify(transactionRepository, times(1)).findAll();
    }

    @Test
    public void testUpdateReturnDatePositive() {
        TransactionEntity te = TransactionEntity.builder()
                .bookEntity(BookEntity.builder().id(1L).build())
                .memberEntity(MemberEntity.builder().id(1L).build())
                .build();
        doReturn(Optional.ofNullable(te)).when(transactionRepository).findById(any());
        Transaction transaction = transactionService.updateReturnDate(1L);
        Assert.assertNotNull(transaction);
        Assert.assertNotNull(transaction.getBook());
        Assert.assertNotNull(transaction.getMember());
        verify(transactionRepository, times(1)).findById(any());
        verify(transactionRepository, times(1)).save(any());
    }

    @Test(expected = NotFoundException.class)
    public void testUpdateReturnDateNotFoundTransaction() {
        doReturn(Optional.ofNullable(null)).when(transactionRepository).findById(any());
        transactionService.updateReturnDate(1L);
    }

    @Test(expected = ForbiddenException.class)
    public void testUpdateReturnDate() {
        TransactionEntity te = TransactionEntity.builder()
                .bookEntity(BookEntity.builder().id(1L).build())
                .memberEntity(MemberEntity.builder().id(1L).build())
                .dateOfReturn(LocalDateTime.now())
                .build();
        doReturn(Optional.ofNullable(te)).when(transactionRepository).findById(any());
        transactionService.updateReturnDate(1L);
    }

    @Test
    public void testSaveByBookIdAndMemberIdPositive() {
        doReturn(TransactionEntity.builder().build()).when(transactionRepository).save(any());
        doReturn(1).when(transactionRepository).countByMemberIdWhereEndDateNotNull(any());
        doReturn(0).when(transactionRepository).countByBookIdWhereEndDateNotNull(any());
        doReturn(Optional.ofNullable(BookEntity.builder().build())).when(bookRepository).findById(any());
        doReturn(Optional.ofNullable(MemberEntity.builder().build())).when(memberRepository).findById(any());
        transactionService.saveByBookIdAndMemberId(1L, 2L);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSaveByBookIdAndMemberIdBookIdIsNull() {
        transactionService.saveByBookIdAndMemberId(null, 2L);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSaveByBookIdAndMemberIdMemberIdIsNull() {
        transactionService.saveByBookIdAndMemberId(2L, null);
    }

    @Test(expected = ForbiddenException.class)
    public void testSaveByBookIdAndMemberIdLimitOfBooksIsExceeded() {
        doReturn(5).when(transactionRepository).countByMemberIdWhereEndDateNotNull(any());
        transactionService.saveByBookIdAndMemberId(1L, 2L);
    }

    @Test(expected = ForbiddenException.class)
    public void testSaveByBookIdAndMemberIdBookIsAlreadtIssued() {
        doReturn(1).when(transactionRepository).countByBookIdWhereEndDateNotNull(any());
        transactionService.saveByBookIdAndMemberId(1L, 2L);
    }

    @Test(expected = NotFoundException.class)
    public void testSaveByBookIdAndMemberIdBookNotFound() {
        doReturn(1).when(transactionRepository).countByMemberIdWhereEndDateNotNull(any());
        doReturn(0).when(transactionRepository).countByBookIdWhereEndDateNotNull(any());
        doReturn(Optional.ofNullable(null)).when(bookRepository).findById(any());
        doReturn(Optional.ofNullable(MemberEntity.builder().build())).when(memberRepository).findById(any());
        transactionService.saveByBookIdAndMemberId(1L, 2L);
    }

    @Test(expected = NotFoundException.class)
    public void testSaveByBookIdAndMemberIdMemberNotFound() {
        doReturn(1).when(transactionRepository).countByMemberIdWhereEndDateNotNull(any());
        doReturn(0).when(transactionRepository).countByBookIdWhereEndDateNotNull(any());
        doReturn(Optional.ofNullable(BookEntity.builder().build())).when(bookRepository).findById(any());
        doReturn(Optional.ofNullable(null)).when(memberRepository).findById(any());
        transactionService.saveByBookIdAndMemberId(1L, 2L);
    }
}
