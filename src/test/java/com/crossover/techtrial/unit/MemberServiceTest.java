package com.crossover.techtrial.unit;


import com.crossover.techtrial.domain.entity.MemberEntity;
import com.crossover.techtrial.domain.entity.TopMemberEntity;
import com.crossover.techtrial.domain.model.Member;
import com.crossover.techtrial.dto.TopMemberDTO;
import com.crossover.techtrial.repositories.MemberRepository;
import com.crossover.techtrial.repositories.TopMemberRepository;
import com.crossover.techtrial.service.MemberService;
import com.crossover.techtrial.service.impl.MemberServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MemberServiceTest {

    private MemberService memberService;

    private MemberRepository memberRepository;

    private TopMemberRepository topMemberRepository;

    @Before
    public void setUp() {
        memberRepository = Mockito.mock(MemberRepository.class);
        topMemberRepository = Mockito.mock(TopMemberRepository.class);
        memberService = new MemberServiceImpl(memberRepository, topMemberRepository);
    }


    @Test
    public void testShouldSavePositive() {
        doReturn(MemberEntity.builder().build()).when(memberRepository).save(any());
        memberService.save(Member.builder().build());
        verify(memberRepository, times(1)).save(any());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testShouldSaveThrowIllegalArgumentException() {
        memberService.save(null);
    }

    @Test
    public void testShouldFindByIdPositive() {
        String name = "John Doe";
        doReturn(Optional.ofNullable(MemberEntity.builder().name(name).build())).when(memberRepository).findById(any());
        Member member = memberService.findById(1L);
        Assert.assertNotNull(member);
        Assert.assertTrue(member.getName().equals(name));
        verify(memberRepository, times(1)).findById(any());
    }

    @Test
    public void testShouldFindByIdPositiveReturnNull() {
        doReturn(Optional.ofNullable(null)).when(memberRepository).findById(any());
        Member member = memberService.findById(1L);
        Assert.assertNull(member);
        verify(memberRepository, times(1)).findById(any());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testShouldFindByIdThrow() {
        memberService.findById(null);
    }

    @Test
    public void testShouldFindAllPositive() {
        List<MemberEntity> list = new ArrayList<>();
        list.add(MemberEntity.builder().build());
        list.add(MemberEntity.builder().build());
        doReturn(list).when(memberRepository).findAll();
        List<Member> members = memberService.findAll();
        Assert.assertNotNull(members);
        Assert.assertTrue(members.size() == 2);
        verify(memberRepository, times(1)).findAll();
    }

    @Test
    public void testShouldFindAllReturnNull() {
        doReturn(null).when(memberRepository).findAll();
        List<Member> members = memberService.findAll();
        Assert.assertNull(members);
        verify(memberRepository, times(1)).findAll();
    }

    @Test
    public void testShouldTopMembersPositive() {
        List<TopMemberEntity> entities = new ArrayList<>();
        entities.add(TopMemberEntity.builder().build());
        entities.add(TopMemberEntity.builder().build());
        entities.add(TopMemberEntity.builder().build());
        doReturn(entities).when(topMemberRepository).findTopMembers(any(), any(), anyInt());
        List<TopMemberDTO> members = memberService.findTopMembers(LocalDateTime.MIN, LocalDateTime.MAX);
        Assert.assertNotNull(members);
        Assert.assertTrue(members.size() == 3);
        verify(topMemberRepository, times(1)).findTopMembers(any(), any(), anyInt());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testShouldTopMembersFailStartDate() {
        memberService.findTopMembers(null, LocalDateTime.MAX);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testShouldTopMembersFailEndDate() {
        memberService.findTopMembers(LocalDateTime.MIN, null);
    }


}
