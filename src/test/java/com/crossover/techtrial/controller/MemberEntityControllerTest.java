/**
 *
 */
package com.crossover.techtrial.controller;

import com.crossover.techtrial.domain.model.Member;
import com.crossover.techtrial.repositories.MemberRepository;
import com.crossover.techtrial.util.HttpUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * @author kshah
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class MemberEntityControllerTest {

    private MockMvc mockMvc;

    @Mock
    private MemberController memberController;

    @Autowired
    private TestRestTemplate template;

    @Autowired
    private MemberRepository memberRepository;

    @Before
    public void setup() throws Exception {
        mockMvc = MockMvcBuilders.standaloneSetup(memberController).build();
    }

    @Test
    public void testMemberRegistrationSuccessful() throws Exception {
        HttpEntity<Object> member = HttpUtil.getHttpEntity(
                "{\"name\": \"test 1\", \"email\": \"test10000000000001@gmail.com\","
                        + " \"membershipStatus\": \"ACTIVE\",\"membershipStartDate\":\"2018-08-08T12:12:12\" }");

        ResponseEntity<Member> response = template.postForEntity(
                "/api/member", member, Member.class);

        Assert.assertEquals("test 1", response.getBody().getName());
        Assert.assertEquals(200, response.getStatusCode().value());

        //cleanup the user
        if (response.getBody().getId() != null) {
            memberRepository.deleteById(response.getBody().getId());
        }
    }

    @Test
    public void testMemberRegsitrationInvalidEmail() throws Exception {
        HttpEntity<Object> member = HttpUtil.getHttpEntity(
                "{\"name\": \"test 1\", \"email\": \"test1000000000000gmail.com\","
                        + " \"membershipStatus\": \"ACTIVE\",\"membershipStartDate\":\"2018-08-08T12:12:12\" }");

        ResponseEntity<Member> response = template.postForEntity(
                "/api/member", member, Member.class);
        Assert.assertEquals(400, response.getStatusCode().value());

        //cleanup the user
        if (response.getBody().getId() != null) {
            memberRepository.deleteById(response.getBody().getId());
        }
    }

    @Test
    public void testMemberRegsitrationInvalidName() throws Exception {
        HttpEntity<Object> member = HttpUtil.getHttpEntity(
                "{\"name\": \"1\", \"email\": \"test10000000000001@gmail.com\","
                        + " \"membershipStatus\": \"ACTIVE\",\"membershipStartDate\":\"2018-08-08T12:12:12\" }");

        ResponseEntity<Member> response = template.postForEntity(
                "/api/member", member, Member.class);
        Assert.assertEquals(400, response.getStatusCode().value());

        //cleanup the user
        if (response.getBody().getId() != null) {
            memberRepository.deleteById(response.getBody().getId());
        }

    }


}
