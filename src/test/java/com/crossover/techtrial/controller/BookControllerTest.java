/**
 *
 */
package com.crossover.techtrial.controller;

import com.crossover.techtrial.domain.model.Book;
import com.crossover.techtrial.repositories.BookRepository;
import com.crossover.techtrial.util.HttpUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * @author kshah
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class BookControllerTest {

    private MockMvc mockMvc;

    @Mock
    private BookController bookController;

    @Autowired
    private TestRestTemplate template;

    @Autowired
    private BookRepository bookRepository;

    @Before
    public void setup() throws Exception {
        mockMvc = MockMvcBuilders.standaloneSetup(bookController).build();
    }

    @Test
    public void testSaveBookPositive() throws Exception {
        HttpEntity<Object> member = HttpUtil.getHttpEntity(
                "{\"title\": \"test 1\"}");

        ResponseEntity<Book> response = template.postForEntity(
                "/api/book", member, Book.class);

        Assert.assertEquals("test 1", response.getBody().getTitle());
        Assert.assertEquals(200, response.getStatusCode().value());

        if (response.getBody().getId() != null) {
            bookRepository.deleteById(response.getBody().getId());
        }
    }


}
